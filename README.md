
![Contao Logo](https://contao.org/files/contao/logo/contao-logo-corporate.svg) | song

## [>> Play now](https://cmichael.de/christianMichaelMusik/ContaoSong.wav) 
The recorded Studio Version

## Live Version

  
[![ContaoSong@ContaoConference2017](https://img.youtube.com/vi/w6z1Bgpg9hQ/0.jpg)](https://www.youtube.com/watch?v=w6z1Bgpg9hQ)
Live at the Contao Conference 2017 in Potsdam Germany
    




## Purpose

This song is for all people who love music and Contao! Feel free to play the Contao Song everywhere you want. Just keep the good intentions!  

## It's open source!

You can use the wav-files to extend the song with your ideas. You can add drums or other instruments to the repository or you can translate the song in different languages.

Therefore you will not only find the hole [ContaoSong.wav](https://cmichael.de/christianMichaelMusik/ContaoSong.wav) at the repository but also the separated vocal track [ContaoSong_onlyVox.wav](https://cmichael.de/christianMichaelMusik/ContaoSong_onlyVox.wav), guitare track [ContaoSong_onlyGit.wav](https://cmichael.de/christianMichaelMusik/ContaoSong_onlyGit.wav) and backing vocals track [ContaoSong_onlyBVS.wav](https://cmichael.de/christianMichaelMusik/ContaoSong_onlyBVS).

Just clone or download the repo. You can do a merge (/pull) request - it's open source :-).

If you like the project or if you have new feature ideas you can write me at  [kontakt@cmichael.de](mailto:kontakt@cmichael.de) or just leave a comment [here](https://www.youtube.com/watch?v=w6z1Bgpg9hQ) on youtube.
                                                                                                                                                                                                                    
## License

The Contao Song is licensed under the terms of the LGPLv3.

## Thanks to

* [Contao Association](https://association.contao.org/) - Yeahhh! Contao rocks!
* [Heartdisco Music](https://www.heartdisco.de/) for recording the [studio version](https://cmichael.de/christianMichaelMusik/ContaoSong.wav) 
* [Nicky Hoff](https://www.hofff.com/de/) for recording the live [video](https://www.youtube.com/watch?v=w6z1Bgpg9hQ)) 
* [Christian Barkowsky](https://brkwsky.de/) for keeping the microphone stand (watch [video](https://www.youtube.com/watch?v=w6z1Bgpg9hQ))
* [Leo Feyer](https://feyermedia.de/) for supporting this song